# Changefly Code Challenge

Code challenge begins!

This application consists in an app that shows Changefly Logo... Yeah... That's all folks!

## Entry point: main.dart

Here we have defined a ```ChangeflyApp``` class that extends from ```StatelessWidget```. This is
because this widget will not change dynamically. Also ```onGenerateRoute``` is setted in order to
show ```HomePage``` Widget.

```
class ChangeflyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Changefly',
      color: Colors.grey[500],
      onGenerateRoute: (RouteSettings setting) {
        return MaterialPageRoute(
          builder: (BuildContext context) => HomePage(),
        );
      },
    );
  }
}
```

## The Magic: HomePage.dart

In this class all the magic happens. Each side of the cube is positioned and also the name 
'Changefly'. It extends from ```StatefulWidget``` because it will change dynamically.
```
class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}
``` 
Also the class ```_HomePageState``` is created in order to handle the Widget states. 
```
class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
    
    //Variables declaration
    

    @override
    void initState() {
        super.initState();
        ... 
        //Variables implementation
        ...   
    }  
}
```

I won't describe the behavior of each side because is always the same and you can see the final 
result in the code.

Each piece of the cube have different animations and also the changefly name. Let's see one of them:

### Example: Top Side of the cube

First of all we must define two variables:

AnimationController will handle our Animation. In this case we are defining a duration of 2 seconds
and the property ```vsync```.
```
AnimationController topSideController;
```
```
topSideController = AnimationController(
  duration: Duration(
    milliseconds: 2000,
  ),
  vsync: this,
);
```

Animation class will define our animation itself. The beginning position, and the end position.
Also we will define the behavior of our animation using ```CurvedAnimation```.
```
Animation<double> topSideAnimation;
```
```
topSideAnimation = Tween(begin: -400.0, end: 0.0).animate(
  CurvedAnimation(
    parent: topSideController,
    curve: Curves.decelerate,
  )
);
```
Then, all we have to do is call the method ```forward()``` of the controller to see the animation.
```
topSideController.forward();
```

Now, out of the ```initState``` method we define another one. This will build our animation and will
do the trick. It will position our image where we want. From top to bottom, left to right, etc.
```
Widget buildTopSideAnimation() {
    return AnimatedBuilder(
      animation: topSideAnimation,
      builder: (context, child) {
        return Positioned(
          child: child,
          top: topSideAnimation.value,
          right: 0.0,
          left: 0.0,
        );
      },
      child: Image.asset('assets/img/changefly-cube-top.png'),
    );
  }
```

Now it is all prepared. Let's see our Animations in action. In the build method we create a simple
but powerful layout:

```
@override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Stack(
              overflow: Overflow.visible,
              children: <Widget>[
                buildTopSideAnimation(),
                buildRightSideAnimation(),
                buildLeftSideAnimation(),
                buildLogoAnimation(),
                Container(
                  width: 200.0,
                  height: 220.0,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
```
All we have to do is call our builders and that's all. 
