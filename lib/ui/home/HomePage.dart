import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with TickerProviderStateMixin {

  Animation<double> topSideAnimation;
  AnimationController topSideController;
  Animation<double> rightSideAnimation;
  AnimationController rightSideController;
  Animation<double> leftSideAnimation;
  AnimationController leftSideController;
  Animation<double> logoAnimation;
  AnimationController logoController;

  @override
  void initState() {
    super.initState();

    topSideController = AnimationController(
      duration: Duration(
        milliseconds: 2000,
      ),
      vsync: this,
    );

    topSideAnimation = Tween(begin: -400.0, end: 0.0).animate(
      CurvedAnimation(
        parent: topSideController,
        curve: Curves.decelerate,
      )
    );

    topSideController.forward();

    rightSideController = AnimationController(
      duration: Duration(
        milliseconds: 2000,
      ),
      vsync: this,
    );

    rightSideAnimation = Tween(begin: -400.0, end: 0.0).animate(
      CurvedAnimation(
        parent: rightSideController,
        curve: Curves.bounceInOut,
      )
    );

    rightSideController.forward();

    leftSideController = AnimationController(
      duration: Duration(
        milliseconds: 2000,
      ),
      vsync: this,
    );

    leftSideAnimation = Tween(begin: -400.0, end: 0.0).animate(
      CurvedAnimation(
        parent: leftSideController,
        curve: Curves.elasticInOut,
      )
    );

    leftSideController.forward();

    logoController = AnimationController(
      duration: Duration(
        milliseconds: 3000,
      ),
      vsync: this,
    );

    logoAnimation = Tween(begin: -800.0, end: 0.0).animate(
      CurvedAnimation(
        parent: logoController,
        curve: Curves.decelerate
      )
    );

    logoController.forward();
  }

  Widget buildTopSideAnimation() {
    return AnimatedBuilder(
      animation: topSideAnimation,
      builder: (context, child) {
        return Positioned(
          child: child,
          top: topSideAnimation.value,
          right: 0.0,
          left: 0.0,
        );
      },
      child: Image.asset('assets/img/changefly-cube-top.png'),
    );
  }

  Widget buildRightSideAnimation() {
    return AnimatedBuilder(
      animation: rightSideAnimation,
      builder: (context, child) {
        return Positioned(
          child: child,
          top: 0.0,
          right: rightSideAnimation.value,
          left: 0.0,
        );
      },
      child: Image.asset('assets/img/changefly-cube-right.png'),
    );
  }

  Widget buildLeftSideAnimation() {
    return AnimatedBuilder(
      animation: leftSideAnimation,
      builder: (context, child) {
        return Positioned(
          child: child,
          top: 0.0,
          right: 0.0,
          left: leftSideAnimation.value,
        );
      },
      child: Image.asset('assets/img/changefly-cube-left.png'),
    );
  }

  Widget buildLogoAnimation(){
    return AnimatedBuilder(
      animation: logoAnimation,
      builder: (context, child) {
        return Positioned(
          child: child,
          bottom: logoAnimation.value - 25,
          right: 0.0,
          left: 0.0,
        );
      },
      child: Image.asset('assets/img/changefly-name.png'),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Stack(
              overflow: Overflow.visible,
              children: <Widget>[
                buildTopSideAnimation(),
                buildRightSideAnimation(),
                buildLeftSideAnimation(),
                buildLogoAnimation(),
                Container(
                  width: 200.0,
                  height: 220.0,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}