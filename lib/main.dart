import 'package:changefly_app/ui/home/HomePage.dart';
import 'package:flutter/material.dart';

void main(){
  runApp(ChangeflyApp());
}

class ChangeflyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Changefly',
      color: Colors.grey[500],
      onGenerateRoute: (RouteSettings setting) {
        return MaterialPageRoute(
          builder: (BuildContext context) => HomePage(),
        );
      },
    );
  }
}